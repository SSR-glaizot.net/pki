# Mise en oeuvre de l'environement

## Prérequis

- virtualbox : https://www.virtualbox.org/wiki/Downloads
- vagrant : https://developer.hashicorp.com/vagrant/downloads

## Installation

Cloner le dépot git : https://gitlab.com/formation131/pki.git

Depuis le dossier pki, lancer la commande suivante : vagrant up

Sur votre machine hôte, configurer en serveur dns secondaire 192.168.56.10

## Infrastructure

L'infrastructure comporte 3 serveurs : enregistré sur le domaine ais-wf3.local

- dns : 192.168.56.10
- pki : 192.168.56.11
- web : 192.168.56.12

Les trois serveurs possèdent un compte admin (pasword : pass) avec les droits sudo 

### dns

Utilisé pour la résolution DNS de domaine sur l'environement

### pki

l'autorité de certification éxecute step-ca (https://smallstep.com/docs/step-ca), en écoute sur le port 8443.
En l'état le service propose un serveur ACME. 

Ce dernier est accessible via https://pki.ais-wf3.local:8443/acme/acme/directory

La CA est configuré pour pouvoir emmetre des certificats SSL

Le certificat racine de cette CA est disponible dans /etc/step-ca/certs

### web

Le serveur web execute un serveur Apache2 configuré en http://web.ais-wf3.local

## Exercice

**L'ensemble des actions que vous effectuerez devra être documenté, un rendu est attendu pour pouvoir vous évaluer sur le module de la semaine**

- Configurer les 3 serveurs pour permettre une connexion en ssh par clé depuis votre machine hôte
- Générer des couples de clée pour les utilisateurs admin de chaque machines
- Configurer la connection par clef SSH entre tout les serveurs
- Mettre en place SSL sur le serveur web à l'aide de Certbot
- Tester la connection en https:// Que constatez vous ? Comment résoudre **proprement le problème rencontré**
- Mettre en place un serveur SFTP (si possible avec CHROOT d'un "sub-directory in home user")


## Rendu (obligatoire) 

- Vous déposerez votre rendu dans ce dépôt https://nc.spoofing.cloudns.pro/s/2YPJgfswHXHAZBf
- Le document devra être au format PDF et disposer des éléments du plan ci-dessous
- Vous fournirez les captures des configurations suivantes: Clef SSH, SFTP, config SSL server web + Page Web https://
- En tant qu'Administrateur d'infrastructure sécurisée, vous préconisez des axes d'améliorations concernant la sécurisation de l'infrastruture.


## Notation & critère d'évaluation

- Clarté du document, sommaire, titre, sous-titre, en-tête, pied de page... [5 points]
- Pouvoir rédactionnel et de synthèse [5 points]
- Captures des configurations + capture du recettage [5 points]
- Respect des consignes et autonomie [5 points]
- Dead-line (20/11/2022 à 11:59 pm) [Eliminatoire > 0/20 si non rendu ou hors délai, rattrapage nécessaire]


# Exemple du plan du rendu 

- Page de garde
- Sommaire
- Contexte
- Public cible
- Infrastructure
- Schéma
- Solution sécurisation SSH 
/ recettage solution
- Solution sécurisation SFTP 
/ recettage solution
- Solution sécurisation HTTPD
/ recettage solution
- Préconisation sécurisation de l'infrastructure
- Conclusion
