#!/bin/bash


DOMAINE=$1
HOSTNAME=$(hostname)
IP=$(hostname -I | cut -d' ' -f2 )

echo 'DEBUT script Install_WEB.sh' $HOSTNAME  $IP

echo "[WEB 1]  Mise a jours resolv.conf"

DNSIP=$(grep DNS /etc/hosts |cut -f1 -d " ")
cat > /etc/resolv.conf << EOF
domain $DOMAINE
search $DOMAINE
nameserver $DNSIP
nameserver 8.8.8.8
EOF

echo "[WEB 2]  Install Apache"


apt install -y -qq apache2 python3-certbot-apache certbot >> /dev/null

cat > /etc/apache2/sites-available/web.conf << EOF 
<VirtualHost *:80>
        ServerName web.ais-wf3.local

        ServerAdmin webmaster@localhost
        DocumentRoot /var/www/html


        ErrorLog ${APACHE_LOG_DIR}/error.log
        CustomLog ${APACHE_LOG_DIR}/access.log combined

</VirtualHost>
EOF


a2dissite 000-default.conf 
a2ensite web.conf 


systemctl restart apache2

