#!/bin/bash


DOMAINE=$1
HOSTNAME=$(hostname)
IP=$(hostname -I | cut -d' ' -f2 )

echo 'DEBUT script Install_stepCA.sh' $HOSTNAME  $IP

echo "[PKI 1]  Mise a jours resolv.conf"

DNSIP=$(grep DNS /etc/hosts |cut -f1 -d " ")
cat > /etc/resolv.conf << EOF
domain $DOMAINE
search $DOMAINE
nameserver $DNSIP
nameserver 8.8.8.8
EOF

echo "[PKI 2] install step-ca"


wget https://dl.step.sm/gh-release/cli/doc-ca-install/v0.19.0/step-cli_0.19.0_amd64.deb

dpkg -i step-cli_0.19.0_amd64.deb 
wget https://dl.step.sm/gh-release/certificates/doc-ca-install/v0.19.0/step-ca_0.19.0_amd64.deb
dpkg -i step-ca_0.19.0_amd64.deb 


echo "[PKI 3] config step-ca"


echo "lepass" > /root/password.txt
step ca init --ssh  --deployment-type=standalone --name="Pki " --dns=pki.$DOMAINE --address=:8443 --provisioner=admin@$DOMAINE --password-file=/root/password.txt  

mv $(step path) /etc/step-ca

sed -i "s/\/root\/.step/\/etc\/step-ca/g" /etc/step-ca/config/defaults.json 
sed -i "s/\/root\/.step/\/etc\/step-ca/g" /etc/step-ca/config/ca.json

useradd --system --home /etc/step-ca --shell /bin/false step

chown -R step:step /etc/step-ca

echo "[PKI 3] confid systemd et démarage"

wget https://raw.githubusercontent.com/smallstep/certificates/master/systemd/step-ca.service
mv step-ca.service /etc/systemd/system/
export STEPPATH=/etc/step-ca
echo STEPPATH=/etc/step-ca > /etc/environment 
systemctl daemon-reload 
systemctl enable --now step-ca
mv /root/password.txt /etc/step-ca/

step ca provisioner add acme --type ACME

systemctl restart step-ca.service
