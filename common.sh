#!/bin/bash


USER="admin"
PASS="pass"

HOSTNAME=$(hostname)
IP=$(hostname -I | cut -d' ' -f2 )
echo 'DEBUT script Common.sh' $HOSTNAME  $IP


echo "[1] update + install utils"
apt-get update -qq > /dev/null
apt-get install -y -qq curl aptitude > /dev/null


echo "[2] ajout user " $USER
useradd -d /home/$USER -s /bin/bash -p pass -m $USER 
echo $USER:$PASS | chpasswd
echo $USER "ALL=(ALL) NOPASSWD:ALL" > /etc/sudoers.d/$USER
systemctl restart sudo 


echo "[3] Acces SSH "
sed -i 's/ChallengeResponseAuthentication no/ChallengeResponseAuthentication yes/g' /etc/ssh/sshd_config
service ssh restart


echo 'FIN script Common.sh' $HOSTNAME  $IP

